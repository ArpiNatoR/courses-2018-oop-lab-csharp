﻿using System;

namespace ExtensionMethods {

    public static class ComplexExtensions
    {
        public static IComplex Add(this IComplex c1, IComplex c2)
        {
            return new Complex(c1.Real + c2.Real, c1.Imaginary + c2.Imaginary);
        }

        public static IComplex Subtract(this IComplex c1, IComplex c2)
        {
            return new Complex(c1.Real - c2.Real, c1.Imaginary - c2.Imaginary);
        }

        public static IComplex Multiply(this IComplex c1, IComplex c2)

        {
            Complex res = new Complex(c1.Real * c2.Real - c1.Imaginary * c2.Imaginary, c1.Real * c2.Imaginary + c1.Imaginary * c2.Real);
            Console.WriteLine("Number: {0} + {1}", res.Real, res.Imaginary);
            return res;
        }

        public static IComplex Divide(this IComplex c1, IComplex c2)
        {
            Complex res = new Complex(((c2.Real * c1.Real + c2.Imaginary * c1.Imaginary) / Math.Pow(c1.Modulus, 2)), (c2.Imaginary * c1.Real - c2.Real * c1.Imaginary) / Math.Pow(c1.Modulus, 2));
            Console.WriteLine("Number: {0} + {1}", res.Real, res.Imaginary);
            return res;
        }

        public static IComplex Conjugate(this IComplex c1)
        {
            throw new NotImplementedException();
        }

        public static IComplex Reciprocal(this IComplex c1)
        {
            throw new NotImplementedException();
        }
    }

}